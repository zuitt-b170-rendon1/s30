/*
	create an express variable that accepts a require ("express") value 
	store the "express()" function inside the "app" variable
	create a port variable that accepts 3000
	make your app able to read json as well as accept data from forms

	make your app listen/run tp the port variable with a confirmation in the console "server is running at port"
*/


let express = require("express");

// mongoose - package that allows creation of schemas to model the data structure and to have an access to a number of methods for manipulating the concrete database in our MongoDB

const mongoose = require ("mongoose");

const app = express(); 

const port = 3000;

// <username> - change into the correct username in your MongoDB account that has ADMIN
// <password> - change into the password of the ADMIN
// "myFirstDatabase" - the name of the database that will be created (changed into "b170-to-do")
mongoose.connect("mongodb+srv://crendon:crendon@wdc028-course-booking.6fjk2.mongodb.net/b170-to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// notification for connection: success/failure
let db = mongoose.connection
// if an error existed in connecting to MongoDB
db.on("error", console.error.bind(console, "Connection Error"));
// if the connection is successful
db.once("open", () => console.log ("We're connected to the database"))


app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Mongoose Schema - sets the structure of the document that is to be created; serves as the blueprint to the data/record
const taskSchema = new mongoose.Schema({
	name:String,
	status:{
		type:String,
		// default - sets the value once the field does not have any value entered in it
		default:"on going"
	}
})

// model - allows access to methods that will perform CRUD operations in the database; RULE: the first letter is always capital/uppercase for the variable of the model and must be singular form
/*
SYNTAX
	const <Variable> = mongoose.model("<Variable>",<schemaName)
*/

const Task = mongoose.model("Task", taskSchema)



// routes
/*
business logic:
1. check if the task is already existing
	if the task exists, return "there is a duplicate task"
	if it is not existing, add the task in the database

2. the task will come from the request body

3. create a new task object with the needed properties

4. save to db
*/

app.listen(port, () => console.log(`Server is running at port ${port}`));

app.post("/tasks", (req,res) => {
	// checking for duplicate task
	// findOne is a mongoose method that acts similar to find in MongoDB; returns the first document it finds
	Task.findOne({name:req.body.name}, (error,result) => {
		// if the "result" has found a task, it will return the res.send
		if (result !== null && result.name ===req.body.name){
			return res.send("There is a duplicate task");
		} else{
			// if no task is found, create the object and save it to the db
			let newTask = new Task ({
				name:req.body.name
			})
			// saveErr - parameter that accepts errors, should there be any when saving the "newTask" object
			// saveTask - parameter that accepts the object should the saving of the "newTask" object is a success
			newTask.save((saveErr, saveTask) => {
				// if there are errors, log in the console the error
				if (saveErr){
					return console.error(saveErr)
				} else {
					// .status - returns a status (number code such as 201 for successful creation)
					return res.status(201).send("New Task Created")
				}
			})
		}
	})
})

// business logic for retrieving data 
/*
1. retrieve all the documents using the find() functionality / tasks route endpoint
2. if an error is encountered, print the error (error handling)
3. if there are no errors, send a success status back to the client and return an array of documents*/



app.get("/tasks", (req, res) => {
	// find is similar to the mongodb/robo3t find. setting up emty field in "{}" would allow the app to find all documents inside the database
	Task.find({}, (error,result) => {
		if (error){
			return console.log(error)
		}
		else{
			return res.status(200).json({data:result}) // data field will bge set and its value will be the result / response that we had for the request
		}
	})
})

// create a function/route for a user registration
/*
1. Find if there is a duplicate user using the username field
	- if the user is existing, return "username already in used"
	- if the user is not existing, add it on the database
		- if the username and the pw are filled, save
			- in saving, create a new User object (a User Schema)
				-if there is an error, log it in the console
				-if there is no error, send a status of 201 and "sucessfully registered"
		- if one of them is blank, send the response "BOTH un and pw must be provided"
*/

const registerSchema = new mongoose.Schema({
	username: String,
	password: String
	
})

const Register = mongoose.model("Register", registerSchema)

app.post("/register", (req,res) => {

	Register.findOne({username:req.body.username}, (error,result) => {
		

		if (result !== null && result.username ===req.body.username){
			return res.send("Username already in used");
		}

		else if (result.username === null || result.password === null){
			return res.send("BOTH username and password must be provided");
		} 

		else{
 
			let newRegister = new Register ({
				username:req.body.username,
				password:req.body.password
			})
			
			newRegister.save((saveErr, saveRegister) => {
				
				if (saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send("Sucessfully Registered")
					
				}
			})
		}
	})
})












// create a get request for getting all registered users
/*
	- retrieve all users registered using find functionality 
	- if there is an error, log it in the console
    - if there is no error, send a success status back to the client and return the array of users
*/

app.get("/regreq", (req, res) => {
	
	Register.find({}, (error,result) => {
		if (error){
			return console.log(error)
		}
		else{
			return res.status(200).json({data:result}) 
		}
	})
})